package x.gui.map;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by kirill on 29.03.17.
 */
public class XOpenMap {

    private static int MAP_SIZE = XMapPanel.MAP_SIZE;

    public static void XOpenMap(JTable table) {
        long[][] data = new long[MAP_SIZE][MAP_SIZE];
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream("src/resources/data/test-data.dat");
            ois = new ObjectInputStream(fis);
            data = (long[][]) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (int y = 0; y < MAP_SIZE; y++) {
            for (int x = 0; x < MAP_SIZE; x++) {
                table.setValueAt(data[y][x], y, x);
            }
        }
    }
}
