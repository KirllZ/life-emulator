package x.gui.map;

import x.data.ucf.XUcfCoder;
import x.gui.main.XMainPanel;
import x.logic.math.XMath;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Random;

@SuppressWarnings("serial")
public class XMapPanel extends JTable {

    public final static int MAP_SIZE = 65;
    public final static int CELL_SIZE = 10;
    public final static int HUMAN_COUNT = 20;
    public final static int PLANT_COUNT = 20;
    private boolean island = true;
    private boolean mask = true;
    private boolean landscape = true;

    public XMapPanel() {
        setupView();
        setupCells();
        setupListeners();
        setVisible(true);
    }

    public void setLandscapeTypeAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeLandscapeType(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 7)), y, x);
    }

    public int getLandscapeTypeAt(int y, int x) {
        return XUcfCoder.decodeLandscapeType(getRawDataAt(y, x));
    }

    public void setHumanTypeAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeHumanType(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 3)), y, x);
    }

    public int getHumanTypeAt(int y, int x) {
        return XUcfCoder.decodeHumanType(getRawDataAt(y, x));
    }

    public void setHumanAgeAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeHumanAge(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 32767)), y, x);
    }

    public int getHumanAgeAt(int y, int x) {
        return XUcfCoder.decodeHumanAge(getRawDataAt(y, x));
    }

    public void setHumanEnergyAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeHumanEnergy(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 63)), y, x);
    }

    public int getHumanEnergyAt(int y, int x) {
        return XUcfCoder.decodeHumanEnergy(getRawDataAt(y, x));
    }

    public void setHumanSatietyAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeHumanSatiety(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 63)), y, x);
    }

    public int getHumanSatietyAt(int y, int x) {
        return XUcfCoder.decodeHumanSatiety(getRawDataAt(y, x));
    }

    public void setHumanPregnancyAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeHumanPregnancy(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 511)), y, x);
    }

    public int getHumanPregnancyAt(int y, int x) {
        return XUcfCoder.decodeHumanPregnancy(getRawDataAt(y, x));
    }

    public void setPlantTypeAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodePlantType(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 1)), y, x);
    }

    public int getPlantTypeAt(int y, int x) {
        return XUcfCoder.decodePlantType(getRawDataAt(y, x));
    }

    public void setPlantFruitsAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodePlantFruits(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 63)), y, x);
    }

    public int getPlantFruitsAt(int y, int x) {
        return XUcfCoder.decodePlantFruits(getRawDataAt(y, x));
    }

    public void setPlantAgeAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodePlantAge(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 512)), y, x);
    }

    public int getPlantAgeAt(int y, int x) {
        return XUcfCoder.decodePlantAge(getRawDataAt(y, x));
    }


    public void setActiveFlagHumanAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeActiveFlagHuman(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 1)), y, x);
    }

    public int getActiveFlagHumanAt(int y, int x) {
        return XUcfCoder.decodeActiveFlagHuman(getRawDataAt(y, x));
    }

    public void setActiveFlagPlantAt(int u, int y, int x) {
        setRawDataAt(XUcfCoder.encodeActiveFlagPlant(getRawDataAt(y, x), XMath.getValueInRange(u, 0, 1)), y, x);
    }

    public int getActiveFlagPlantAt(int y, int x) {
        return XUcfCoder.decodeActiveFlagPlant(getRawDataAt(y, x));
    }

    public void setRawDataAt(long uc, int y, int x) {
        setValueAt(uc, y, x);
    }

    public long getRawDataAt(int y, int x) {
        return (long) getValueAt(y, x);
    }

    public void reset() {
        XOpenMap.XOpenMap(this);
    }

    public void refresh(boolean island, boolean mask,boolean landscape) {
        this.island = island;
        this.mask = mask;
        this.landscape = landscape;
        generateMap();
    }

    private void setupView() {
        setModel(new DefaultTableModel(MAP_SIZE, MAP_SIZE) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        setRowHeight(CELL_SIZE);
        for (int i = 0; i < MAP_SIZE; i++) {
            this.getColumnModel().getColumn(i).setMinWidth(CELL_SIZE);
            this.getColumnModel().getColumn(i).setMaxWidth(CELL_SIZE);
        }
        setBorder(BorderFactory.createLineBorder(new Color(0x333333)));
        setDefaultRenderer(Object.class, new XCellRenderer());
        try {
            Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File("src/resources/gui/fonts/map-units.ttf")).deriveFont(7f);
            setFont(customFont);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupCells() {
//        XOpenMap.XOpenMap(this);
        generateMap();

    }

    private void setupListeners() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = rowAtPoint(e.getPoint());
                int y = columnAtPoint(e.getPoint());
                XMainPanel.cellInfoPanel.update(x, y);
            }
        });
    }

    private Random random = new Random();

    private void generateMap() {
        setModel(new DefaultTableModel(MAP_SIZE, MAP_SIZE));
        setRowHeight(CELL_SIZE);
        for (int i = 0; i < MAP_SIZE; i++) {
            getColumnModel().getColumn(i).setMinWidth(CELL_SIZE);
            getColumnModel().getColumn(i).setMaxWidth(CELL_SIZE);
        }
        for (int x = 0; x < MAP_SIZE; x++) {
            for (int y = 0; y < MAP_SIZE; y++) {
                setValueAt(0x0000_0000_0000_0000L, y, x);
            }
        }

        if (landscape) {
            generateLandscape();
        } else {
            generateLandscape1();
        }
        if (mask) {

            multiplicationByMask1();
        } else {
            multiplicationByMask();
            multiplicationByMask();

        }
        generateHumen();
        generatePlant();
        setVisible(true);
    }

    private final void generateHumen() {

        int counter = 0;
        for (int i = 0; i < HUMAN_COUNT; ) {
            counter++;
            int x = random.nextInt(MAP_SIZE);
            int y = random.nextInt(MAP_SIZE);
            long val = (long) getValueAt(y, x);
            long humanType = XUcfCoder.decodeHumanType((long) getHumanTypeAt(y, x));
            if (val != 0 && val != 1 && val != 2 && humanType == XUcfCoder.HUMAN_TYPE_EMPTY) {
                if (i > HUMAN_COUNT / 2) {
                    setHumanTypeAt(1, y, x);
                } else {
                    setHumanTypeAt(2, y, x);
                }
                setHumanAgeAt(0, y, x);
                setHumanEnergyAt(64, y, x);
                setHumanSatietyAt(64, y, x);
                setHumanPregnancyAt(0, y, x);
                i++;
            }
            if (counter > HUMAN_COUNT * 100) {
                break;
            }
        }
    }

    private final void generatePlant() {
        int counter = 0;
        for (int i = 0; i < PLANT_COUNT; ) {
            counter++;
            int x = random.nextInt(MAP_SIZE);
            int y = random.nextInt(MAP_SIZE);
            long val = (long) getValueAt(y, x);
            if (val != 0 && val != 1 && val != 2) {
                setPlantAgeAt(1, y, x);
                setPlantTypeAt(1, y, x);
                setPlantFruitsAt(random.nextInt(30), y, x);
                i++;
            }
            if (counter > PLANT_COUNT * 100) {
                break;
            }

        }
    }

    private final void generateLandscape() {

        setLandscapeTypeAt(XUcfCoder.LANDSCAPE_TYPE_GROUND_HIGH, 0, 0);
        setLandscapeTypeAt(XUcfCoder.LANDSCAPE_TYPE_GRASS_HIGH, 0, MAP_SIZE - 1);
        setLandscapeTypeAt(XUcfCoder.LANDSCAPE_TYPE_WATER_HIGH, MAP_SIZE - 1, 0);
        setLandscapeTypeAt(XUcfCoder.LANDSCAPE_TYPE_GROUND_LOW, MAP_SIZE - 1, MAP_SIZE - 1);

        float seaAltitude = 3.5F;
        float landscapeShift = 6.0f;
        for (int bigStep = MAP_SIZE - 1; bigStep >= 2; bigStep /= 2, landscapeShift /= 2.0f) {
            int smallStep = bigStep / 2;
            // Diamond step - build diamonds
            for (int x = smallStep; x < MAP_SIZE; x += bigStep) {
                for (int y = smallStep; y < MAP_SIZE; y += bigStep) {
//                    long topLeftValue = (long) getValueAt(y - smallStep, x - smallStep);
//                    long topRightValue = (long) getValueAt(y - smallStep, x + smallStep);
//                    long bottomLeftValue = (long) getValueAt(y + smallStep, x - smallStep);
//                    long bottomRightValue = (long) getValueAt(y + smallStep, x + smallStep);
//                    float avg = (topLeftValue + topRightValue + bottomLeftValue + bottomRightValue) / seaAltitude;
                    long centerValue = getValueInRange(1 + random.nextInt(6), 1, 6);
                    //  long centerValue =getValueInRange(avg , 1, 6);
                    setValueAt(centerValue, y, x);
                }
            }
            // Square step - build squares
            for (int x = 0; x < MAP_SIZE; x += smallStep) {
                for (int y = (x + smallStep) % bigStep; y < MAP_SIZE; y += bigStep) {
                    long topValue = (long) getValueAt((y - smallStep + MAP_SIZE - 1) % (MAP_SIZE - 1), x);
                    long leftValue = (long) getValueAt(y, (x - smallStep + MAP_SIZE - 1) % (MAP_SIZE - 1));
                    long rightValue = (long) getValueAt(y, (x + smallStep) % (MAP_SIZE - 1));
                    long bottomValue = (long) getValueAt((y + smallStep) % (MAP_SIZE - 1), x);
                    float avg = (topValue + leftValue + rightValue + bottomValue) / seaAltitude;
//                    long centerValue = getValueInRange(avg, 1, 6);/* getValueInRange(avg + random.nextInt(3) * landscapeShift - landscapeShift, 1, 6);*/
                    long centerValue = getValueInRange(1 + random.nextInt(6), 1, 6);
                    setValueAt(centerValue, y, x);
                }
            }
        }
    }

    public final void createIsland() {
        for (int x = 0; x < MAP_SIZE; x++) {
            for (int y = 0; y < MAP_SIZE; y++) {
                if (y < 3 || x < 3 || y > MAP_SIZE - 5 || x > MAP_SIZE - 5) {
                    setValueAt((long) 1, y, x);
                }
            }
        }

    }

    public final void multiplicationByMask() {
        long result = 0;
        if (island) {
            createIsland();
        }
        for (int x = 1; x < MAP_SIZE - 1; x++) {
            for (int y = 1; y < MAP_SIZE - 1; y++) {
                result = ((long) getValueAt(x - 1, y - 1)+ (long) getValueAt(x, y - 1) + (long) getValueAt(x + 1, y - 1)
                        + (long) getValueAt(x - 1, y) + (long) getValueAt(x, y) + (long) getValueAt(x + 1, y)
                        + (long) getValueAt(x - 1, y + 1) + (long) getValueAt(x, y + 1) + (long) getValueAt(x + 1, y + 1))/9 ;
                setValueAt(result, y, x);
            }
        }
    }

    public final void multiplicationByMask1() {
        long result = 0;
        if (island) {
            createIsland();
        }
        for (int x = 2; x < MAP_SIZE - 2; x++) {
            for (int y = 2; y < MAP_SIZE - 2; y++) {
                result = ((long) getValueAt(x - 2, y - 2) + (long) getValueAt(x - 1, y - 2) +
                        (long) getValueAt(x, y - 2) + (long) getValueAt(x + 1, y - 2) +
                        (long) getValueAt(x + 2, y - 2) + (long) getValueAt(x - 2, y - 1) +
                        (long) getValueAt(x - 1, y - 1) + (long) getValueAt(x, y - 1) +
                        (long) getValueAt(x + 1, y - 1) + (long) getValueAt(x + 2, y - 1) +
                        (long) getValueAt(x - 2, y) + (long) getValueAt(x - 1, y) +
                        (long) getValueAt(x, y) + (long) getValueAt(x + 1, y) +
                        (long) getValueAt(x + 2, y) + (long) getValueAt(x - 2, y + 1) +
                        (long) getValueAt(x - 1, y + 1) + (long) getValueAt(x, y + 1) +
                        (long) getValueAt(x + 1, y + 1) + (long) getValueAt(x + 2, y + 1) +
                        (long) getValueAt(x - 2, y + 2) + (long) getValueAt(x - 1, y + 2) +
                        (long) getValueAt(x, y + 2) + (long) getValueAt(x + 1, y + 2) +
                        (long) getValueAt(x + 2, y + 2)) / 25;
                setValueAt(result, y, x);
            }
        }
    }

    private long getValueInRange(float value, int min, int max) {
        return (long) Math.max(Math.min(value, max), min);
    }


    public final void generateLandscape1() {

        long arrLandscape[][] = new long[MAP_SIZE][MAP_SIZE];
        arrLandscape[0][0] = arrLandscape[0][MAP_SIZE - 1] = arrLandscape[MAP_SIZE - 1][0] = arrLandscape[MAP_SIZE - 1][MAP_SIZE - 1] = 250;
        float landscapeShift = 500f;
        for (int bigStep = MAP_SIZE - 1; bigStep >= 2; bigStep /= 2, landscapeShift /= 2.0f) {
            int smallStep = bigStep / 2;
            // Diamond step - build diamonds
            for (int x = smallStep; x < MAP_SIZE; x += bigStep) {
                for (int y = smallStep; y < MAP_SIZE; y += bigStep) {
                    long topLeftValue = arrLandscape[y - smallStep][x - smallStep];
                    long topRightValue = arrLandscape[y - smallStep][x + smallStep];
                    long bottomLeftValue = arrLandscape[y + smallStep][x - smallStep];
                    long bottomRightValue = arrLandscape[y + smallStep][x + smallStep];
                    float avg = (topLeftValue + topRightValue + bottomLeftValue + bottomRightValue) / 4;
                    long centerValue = 0;
                    if(random.nextBoolean()) {
                    centerValue = getValueInRange(avg + random.nextInt(50), 50, 600);
                    }else {
                        centerValue=getValueInRange(avg - random.nextInt(50), 50, 600);
                    }
//                    centerValue = getValueInRange(centerValue, 1, 6);
                    arrLandscape[y][x] = centerValue;
                }
            }
            // Square step - build squares
            for (int x = 0; x < MAP_SIZE; x += smallStep) {
                for (int y = (x + smallStep) % bigStep; y < MAP_SIZE; y += bigStep) {
                    long topValue = arrLandscape[(y - smallStep + MAP_SIZE - 1) % (MAP_SIZE - 1)][x];
                    long leftValue = arrLandscape[y][(x - smallStep + MAP_SIZE - 1) % (MAP_SIZE - 1)];
                    long rightValue = arrLandscape[y][(x + smallStep) % (MAP_SIZE - 1)];
                    long bottomValue = arrLandscape[(y + smallStep) % (MAP_SIZE - 1)][x];
                    float avg = (topValue + leftValue + rightValue + bottomValue) / 4;
                    long centerValue = 0;
                    if(random.nextBoolean()) {
                    centerValue = getValueInRange(avg + random.nextInt(50), 50, 600);
                    }else {
                        centerValue=getValueInRange(avg - random.nextInt(50), 50, 600);
                    }
                    arrLandscape[y][x] = centerValue;
                }
            }
        }

        for (int i = 0; i < MAP_SIZE; i++) {
            for (int j = 0; j < MAP_SIZE; j++) {
                setValueAt((long) Math.round(arrLandscape[i][j] / 100), i, j);
            }
        }
    }
}