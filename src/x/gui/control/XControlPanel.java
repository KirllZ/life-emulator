package x.gui.control;

import x.gui.main.XMainPanel;
import x.logic.force.XForce;
import x.logic.statistic.XStatistic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class XControlPanel extends JPanel {

    private JButton startButton;
    private JButton pauseButton;
    private JButton stopButton;
    private JCheckBox maskCheckBox;
    private JCheckBox landscapeCheckBox;
    private JCheckBox islandCheckBox;

    public XControlPanel() {
        setupView();
        setupButtons();
        setupCheckBox();

        setVisible(true);
    }

    private void setupView() {
        setLayout(new FlowLayout(FlowLayout.CENTER));
    }

    private void setupCheckBox() {

        //Generate Landscape
        this.landscapeCheckBox = new JCheckBox();
        landscapeCheckBox.setText("Landscape");
        landscapeCheckBox.setPreferredSize(new Dimension(125, 25));
        landscapeCheckBox.setSelected(true);
        add(landscapeCheckBox);

        //Generate Map with masc
        this.maskCheckBox = new JCheckBox();
        maskCheckBox.setText("Smoothing x2");
        maskCheckBox.setPreferredSize(new Dimension(125, 25));
        maskCheckBox.setSelected(true);
        add(maskCheckBox);

        //Generate Map island
        this.islandCheckBox = new JCheckBox();
        islandCheckBox.setText("Island");
        islandCheckBox.setPreferredSize(new Dimension(150, 25));
        islandCheckBox.setSelected(true);
        add(islandCheckBox);
    }

    private void setupButtons() {
        // START
        this.startButton = new JButton();
        startButton.setIcon(new ImageIcon("src/resources/gui/icons/control-start.png"));
        startButton.setPreferredSize(new Dimension(25, 25));
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XForce.start();
                startButton.setEnabled(false);
                pauseButton.setEnabled(true);
                stopButton.setEnabled(false);
            }
        });
        add(startButton);
        // PAUSE
        this.pauseButton = new JButton();
        pauseButton.setIcon(new ImageIcon("src/resources/gui/icons/control-pause.png"));
        pauseButton.setPreferredSize(new Dimension(25, 25));
        pauseButton.setEnabled(false);
        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XForce.pause();
                startButton.setEnabled(true);
                pauseButton.setEnabled(false);
                stopButton.setEnabled(true);
            }
        });
        add(pauseButton);
        // STOP
        this.stopButton = new JButton();
        stopButton.setIcon(new ImageIcon("src/resources/gui/icons/control-stop.png"));
        stopButton.setPreferredSize(new Dimension(25, 25));
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XForce.stop();
                XStatistic.reset();
                XMainPanel.mapPanel.reset();
                XMainPanel.mapInfoPanel.reset();
                XMainPanel.eventsInfoPanel.reset();
                XMainPanel.cellInfoPanel.reset();
            }
        });
        add(stopButton);
        //REFRESH MAP
        this.stopButton = new JButton();
        stopButton.setIcon(new ImageIcon("src/resources/gui/icons/control-refresh.png"));
        stopButton.setPreferredSize(new Dimension(25, 25));
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XForce.refresh();
                XStatistic.reset();
                XMainPanel.mapPanel.refresh(islandCheckBox.isSelected(), maskCheckBox.isSelected(), landscapeCheckBox.isSelected());
                XMainPanel.mapInfoPanel.reset();
                XMainPanel.eventsInfoPanel.reset();
                XMainPanel.cellInfoPanel.reset();
            }
        });
        add(stopButton);
    }

}
